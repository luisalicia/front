package com.zetcode.service;
import  com.zetcode.model.*;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class MovieService {

		public static MovieType getMoviebyid(int num) {
	    	WebTarget resource = ClientBuilder.newClient().target("http://localhost:9090/getMoviebyid/"+num);
	        return  resource.request(MediaType.APPLICATION_JSON).get(MovieType.class);
	    }      
		public static MovieType delete(int num) {
	    	WebTarget resource = ClientBuilder.newClient().target("http://localhost:9090/deletemovie/"+num);
	        return  resource.request(MediaType.APPLICATION_JSON).delete(MovieType.class);
	    }  
	    public static void Agregar(Movie obj) {
	    	WebTarget resource = ClientBuilder.newClient().target("http://localhost:9090/addMovie");
	          resource.request(MediaType.APPLICATION_JSON).post(Entity.json(obj));
	    } 
	    public static void modificar(Movie obj) {
	    	WebTarget resource = ClientBuilder.newClient().target("http://localhost:9090/update");
	          resource.request(MediaType.APPLICATION_JSON).put(Entity.json(obj));
	    } 
		public static MovieList ConsultarAll() {
	    	WebTarget resource = ClientBuilder.newClient().target("http://localhost:9090/getMovieAll");
	        return  resource.request(MediaType.APPLICATION_JSON).get(MovieList.class);
	    }    
}
