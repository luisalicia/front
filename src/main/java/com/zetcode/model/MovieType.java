package com.zetcode.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MovieType {

 
	public Movie getMovieType() {
		return movieType;
	}

	public void setMovieType(Movie movieType) {
		this.movieType = movieType;
	}

	public Movie  movieType;


	
}
