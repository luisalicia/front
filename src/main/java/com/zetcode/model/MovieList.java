package com.zetcode.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MovieList {
	
	 public List<Movie> movieType;

	    public List<Movie> getMovieType() {
	        return movieType;
	    }

	    public void setMovieType(List<Movie> movieType) {
	    	
	    	this.movieType = movieType;
	    }

}
