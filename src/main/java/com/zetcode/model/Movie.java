package com.zetcode.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Movie {
	
	
	public long movieId;
	public String title;
	public String category;
	public long getMovieId() {
		return movieId;
	}
	public void setMovieId(long movieId) {
		this.movieId = movieId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("movieId: ").append(movieId)
                .append("title: ").append(title)
                .append("category ").append(category);
                
        return sb.toString();
    }

}
