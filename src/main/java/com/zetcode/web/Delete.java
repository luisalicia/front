package com.zetcode.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.InternalServerErrorException;

import com.zetcode.service.MovieService;

@WebServlet(name = "Delete", urlPatterns = {"/Delete"})
public class Delete  extends HttpServlet{

	private static final long serialVersionUID = 1L;
	@Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		  
        request.getRequestDispatcher("/vistas/delete.jsp").forward(request, response);
    }
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		 request.getRequestDispatcher("/vistas/delete.jsp").forward(request, response);
			
    }
	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		String id =  request.getParameter("idmovie");
		if(esNumero(id)==true) {   
		try {
			  MovieService.delete(Integer.parseInt(id));
			  request.setAttribute("correcto", "Se elimino correctamente");
			  request.getRequestDispatcher("/vistas/delete.jsp").forward(request, response);
		} catch (InternalServerErrorException e) {
			  System.out.println(e);
			  request.setAttribute("error", "Dato eliminado");
			  request.getRequestDispatcher("/vistas/delete.jsp").forward(request, response);
		}
		}else {
			  request.setAttribute("expcion", "El dato Ingresado es incorrecto");
			  request.getRequestDispatcher("/vistas/delete.jsp").forward(request, response);
		}
	}
	 public static boolean esNumero(String cadena) {

	        boolean resultado;

	        try {
	            Integer.parseInt(cadena);
	            resultado = true;
	        } catch (NumberFormatException excepcion) {
	            resultado = false;
	        }

	        return resultado;
	    }
}
