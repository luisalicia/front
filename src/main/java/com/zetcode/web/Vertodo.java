package com.zetcode.web;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.ProcessingException;

import com.zetcode.model.MovieList;
import com.zetcode.service.MovieService;

@WebServlet(name = "Vertodo", urlPatterns = {"/Vertodo"})
public class Vertodo extends HttpServlet {

	private static final long serialVersionUID = 1L;
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
	    try {
	    	MovieList movies = MovieService.ConsultarAll();
			
			System.out.print(movies.getMovieType());
			request.setAttribute("movies", movies);
	        
	        request.getRequestDispatcher("/vistas/showAll.jsp").forward(request, response);
		} catch (ProcessingException e) {
			System.out.println(e);
			request.getRequestDispatcher("/error/Error500.html").forward(request, response);
		}
		
    }
}
