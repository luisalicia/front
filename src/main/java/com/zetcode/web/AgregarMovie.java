package com.zetcode.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zetcode.model.Movie;
import com.zetcode.service.MovieService;

@WebServlet(name = "AgregarMovie", urlPatterns = {"/AgregarMovie"})
public class AgregarMovie extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) 
    		throws ServletException, IOException {
		String MovieName = request.getParameter("pelicula");
		String categoria = request.getParameter("categoria");
		 if (!categoria.equals("") && !MovieName.equals("")) {
				
				
				Movie obj = new Movie();
				obj.setCategory(categoria);
				obj.setTitle(MovieName);
				System.out.println(obj.getCategory() +"  "+ obj.getTitle());
				MovieService.Agregar(obj);
				request.setAttribute("correcto", "Datos guardados correctamente");
				request.getRequestDispatcher("/vistas/AgregarMovie.jsp").forward(request, response);
				 	
	        } else {
	        	System.out.println("Datos nuloss");
	        	 request.setAttribute("error", "Existe un error al guardar los datos ");
	        	request.getRequestDispatcher("/vistas/AgregarMovie.jsp").forward(request, response);
	        }
    }
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
    		throws ServletException, IOException {
		request.getRequestDispatcher("/vistas/AgregarMovie.jsp").forward(request, response);
	}

}
