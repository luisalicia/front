package com.zetcode.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.InternalServerErrorException;
import com.zetcode.model.MovieType;
import com.zetcode.service.MovieService;

@WebServlet(name = "MyServlet", urlPatterns = {"/MyServlet"})
public class MyServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;


	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/vistas/show.jsp").forward(request, response);
    }
	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
           String id = request.getParameter("id");
           
           if(esNumero(id)==true) {
        	   try {
        		   MovieType people = MovieService.getMoviebyid(Integer.parseInt(id));
                   request.setAttribute("people", people);
                   request.setAttribute("correcto", "Busqueda existosa");
                    request.getRequestDispatcher("/vistas/show.jsp").forward(request, response);
			} catch (InternalServerErrorException e) {
				request.setAttribute("expcion", "El id dado no existe");
	        	request.getRequestDispatcher("/vistas/show.jsp").forward(request, response);
				System.out.println("error en "+e);
			}
          
           } else {
        	   request.setAttribute("error", "El dato ingresado no es correcto");
        	   request.getRequestDispatcher("/vistas/show.jsp").forward(request, response);
           }
    }
	
	 public static boolean esNumero(String cadena) {

	        boolean resultado;

	        try {
	            Integer.parseInt(cadena);
	            resultado = true;
	        } catch (NumberFormatException excepcion) {
	            resultado = false;
	        }

	        return resultado;
	    }
}
