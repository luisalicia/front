<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
	 <style type=text/css> 
         .error{ 
         color: red; 
         text-decoration: underline; 
         font-weight: bold; 
         font-size: 26px; 
         }
          .correcto{ 
         color: green; 
         text-decoration: underline; 
         font-weight: bold; 
         font-size: 26px; 
         }  
        
      </style>
		<meta charset="ISO-8859-1">
		<title>Insert title here</title>
	</head>
	<body>
		<h1>Agregar</h1>
		<form method="post"  action="AgregarMovie" name="add"  >
			nombre de pelicula:<input name="pelicula"/>
			categoria:<input name="categoria"/>
			<input type="Submit" name="save" value="Guardar"/>
			
			<span class="error">${error}</span>
			<span class="correcto" >${correcto}</span>			
		</form>
	</body>
</html>