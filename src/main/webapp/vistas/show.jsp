<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Buscar Personas por id</title>
         <style type=text/css> 
         .error{ 
         color: red; 
         text-decoration: underline; 
         font-weight: bold; 
         font-size: 26px; 
         }
          .correcto{ 
         color: green; 
         text-decoration: underline; 
         font-weight: bold; 
         font-size: 26px; 
         }  
            .expcion{ 
         color: orange; 
         text-decoration: underline; 
         font-weight: bold; 
         font-size: 26px; 
         } 
      </style>
    </head>
    <body>
    <form action="MyServlet" method="post">
    		id de la pelicula:<input name="id"/>
    		<input type="Submit" name="save" value="Buscar"/>
    		<span class="expcion">${expcion}</span>
    		<span class="error">${error}</span>
    		<span class="correcto">${correcto}</span>
    </form>
        <table border="1">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>pelicula</th>
                    <th>categoria</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                        <td><c:out value="${people.movieType.movieId}"/></td>
                        <td><c:out value="${people.movieType.title}"/></td>
                        <td><c:out value="${people.movieType.category}"/></td>
                    </tr>
              
            </tbody>
        </table>
    </body>    
</html>