<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Eliminar Registro</title>
       <style type=text/css> 
         .error{ 
         color: red; 
         text-decoration: underline; 
         font-weight: bold; 
         font-size: 26px; 
         }
          .correcto{ 
         color: green; 
         text-decoration: underline; 
         font-weight: bold; 
         font-size: 26px; 
         }  
            .expcion{ 
         color: orange; 
         text-decoration: underline; 
         font-weight: bold; 
         font-size: 26px; 
         } 
      </style>
</head>
<body>
	<h1>Eliminar</h1>
	  <form action="Delete" method="post">
    		Id de la pelicula:<input name="idmovie"/>
    		<input type="Submit" name="delete" value="Eliminar"/>
    		
    		<span class="expcion">${expcion}</span>
    		<span class="error">${error}</span>
    		<span class="correcto">${correcto}</span>
  
          </form>
</body>
</html>